package com.app;

import org.h2.mvstore.MVMap;
import org.h2.mvstore.MVStore;

/**
 * Zachary Hayes
 * Mumo Systems
 */
 
public class App 
{
    private static final String BASE_URL = "/Users/zacharyhayes/development/amps-standalone-jira-7.6.3"; // Set to JIRA directory.

    public static void main(final String[] args) {
        // open the store (in-memory if fileName is null)
        final MVStore store = MVStore.open(BASE_URL + "/target/jira/home/database/h2db.mv.db");

        final MVMap<Object, Object> openMap = store.openMap("undoLog");

        openMap.clear();

        // close the store (this will persist changes)
        store.close();

        System.out.println("Finished.");
    }
}
